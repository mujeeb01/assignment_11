package org.antwalk.Student;

import java.util.List;

import javax.sql.DataSource;

public interface StudentDao {

	    void setDataSource(DataSource var1);

	    void create(String var1, Integer var2);

	    Student getStudent(Integer var1);

	    List <Student> listStudents();

	    void delete(Integer var1);

	    void update(Integer var1, Integer var2);
	}


